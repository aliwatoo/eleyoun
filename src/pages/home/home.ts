import { Component } from '@angular/core';
import { NavController ,NavParams} from 'ionic-angular';
import {SettingsPage} from '../../pages/settings/settings';
import {AboutPage} from '../../pages/about/about';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { LocalNotifications } from '@ionic-native/local-notifications';
import {  AlertController, Platform } from 'ionic-angular';
import { NotificationProvider } from '../../providers/notification/notification';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public storage: Storage,public provider :NotificationProvider,public navCtrl: NavController , public NavParams :NavParams , private iab: InAppBrowser , private localNotifications: LocalNotifications) {
    
    
    
  

  }
  ionViewDidLoad() {
    
    
    this.storage.get('not').then(data => {
      let objFromString = JSON.parse(data);
      if (data !== null && data !== undefined) {
        
      } 
      else {
        
        this.provider.dat().subscribe(data => {
      console.log(data);
        for (let i =0 ; i < data.length ; i++)
        {
          this.storage.set('not', "1");
          console.log("not");
          this.localNotifications.schedule({
            id: i,
            title: data[i].title,
             at: new Date(new Date().getTime() +600*1000),
            every: "day"

          });
          
        }
    });
        
      }
    });
    
  }

  opensettings()
  {
    
    this.navCtrl.push(SettingsPage);

   
  }
  public openArticle(url: string) {
    this.iab.create(url, '_blank');
     
  }
  openabout()
  {
    this.navCtrl.push(AboutPage);
  }
  open(){

    
  }
}
